\documentclass[12pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper,margin=1in}                   		% ... or a4paper or a5paper or ... 
%\geometry{landscape}                		% Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}				% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage[hidelinks]{hyperref}

\title{Symbolic derivation of wave equation operators}
\author{Huy Le}
%\date{}							% Activate to display a given date or no date

\begin{document}
\maketitle

Define the wave equation, the observed data, and the FWI objective function as following:
\begin{equation}
L=\partial_t^2-m\nabla^2,
\label{eq:waveop}
\end{equation}
\begin{equation}
Lu=s,
\label{eq:waveeq}
\end{equation}
\begin{equation}
d=Ru=\begin{cases}
     u(x_r,t) \mbox{ for } x=x_r,\\
     0 \mbox{ otherwise},
     \end{cases}
\label{eq:data}
\end{equation}
\begin{equation}
f(m)=\frac{1}{2}(d-d_0)^2,
\label{eq:obj}
\end{equation}
where $m$ is the model (velocity square), $s$ is the source, $d$ is the modeled data, $R$ is the recording operator that records the wavefield, $u$, at receivers, and $d_0$ is the observed data. 
\par
For the acoustic isotropic wave equations, when discretized with centered finite differences, the wave equation operator is symmetric, $L^T=L$. Additionally, with the above definition of the modeled data, $R$ is a square diagonal matrix whose nonzero diagonal elements are ones at receiver locations, hence $R^T=R$ and $R^2=R$. 

\section{Born operator}
The Born operator, $B$, relates changes in model to changes in modeled data:
\begin{equation}
\delta d=\frac{\partial d}{\partial m}\delta m=B\delta m.
\label{eq:born}
\end{equation}
Take derivative of both sides of equation \ref{eq:waveeq} with respect to $m$ and multiply both sides with $\delta m$:
\begin{equation}
\frac{\partial L}{\partial m}u\delta m+L\frac{\partial u}{\partial m}\delta m=0.
\end{equation}
Realize that $\frac{\partial L}{\partial m}=-\nabla^2$ and $\frac{\partial u}{\partial m}\delta m=\delta u$ to arrive at the linearized/secondary forward wave equation:
\begin{equation}
L\delta u=\nabla^2u\delta m.
\label{eq:borneq}
\end{equation}
This equation says that the secondary/scattered forward wavefield $\delta u$ is the result of scattering the forward wavefield $u$ off model perturbation $\delta m$. 
\par
Now:
\begin{equation}
B\delta m=\delta d=R\delta u=RL^{-1}\nabla^2u\delta m,
\label{eq:born1}
\end{equation}
and therefore the Born operator is:
\begin{equation}
B=\frac{\partial d}{\partial m}=RL^{-1}\nabla^2u.
\label{eq:bornop}
\end{equation}

\section{Gradient}
Take derivative of equation \ref{eq:obj} with respect to $m$:
\[
\nabla_m f=\left( \frac{\partial d}{\partial m}\right)^T (d-d_0)=B^T(d-d_0)=\nabla^2u \cdot L^{-T}R(d-d_0).
\]
Define an adjoint wavefield $v=L^{-T}R(d-d_0)$ to be solution of the adjoint equation:
\begin{equation}
L^Tv=R(d-d_0).
\label{eq:adjointeq}
\end{equation}
The gradient of the objective function now becomes the dot product (or zero-lag crosscorrelation) of the forward and adjoint wavefields:
\begin{equation}
\nabla_m f=\nabla^2u \cdot v.
\label{eq:deriv1st}
\end{equation}
Because the source term of the adjoint equation \ref{eq:adjointeq} is the injection of data residual at receiver locations, the adjoint wavefield will be radiating outward from the receivers. As a result, crosscorrelation of the forward and adjoint wavefields generates a sensitivity zone connecting the sources and the receivers (Figure \ref{fig:f1}). A perturbation in this zone will have first-order effect on the modeled data. Because the gradient only accounts for first-order scattering, while the source term for the adjoint equation (the residual, equation \ref{eq:adjointeq}) contains all scattering orders, waveform inversion with gradient update is susceptible to cycle skipping, or conversely, when the first-order scattering dominates, waveform inversion is less susceptible to cycle skipping. The first-order scattering is dominant when either the model perturbation is small or the wavelength is large (i.e. low frequency):
\begin{equation}
\frac{S}{\lambda}\frac{\delta m}{m} \ll 1,
\label{eqxx}
\end{equation}
where $S$ is the spatial extension of perturbation $\delta m$ and $\lambda$ is the wavelength. This explains why low frequencies are needed to avoid cycle skipping.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{f1}
\caption{Sensitivity zone from the gradient. Figure taken from \cite{Fichtner}.}
\label{fig:f1}
\end{figure}

\section{Hessians}
The Hessian is the second derivative of the objective function:
\begin{equation}
H=\frac{\partial^2f}{\partial m^2}.
\label{eq:hess}
\end{equation}
For waveform inversion, the application of the Hessian to a model perturbation, $H\delta m$, is needed. By definition, this is the change in gradient due to changes in the model, which can be computed by applying chain rules to equation \ref{eq:deriv1st}:
\begin{equation}
H\delta m=\delta(\nabla_mf)=\nabla^2\delta u \cdot v + \nabla^2u \cdot \delta v.
\label{eq:hessap}
\end{equation}
\par
Three out of four wavefields in the above equation are known already: $\delta u$, $v$, and $u$. To derive the last wavefield, $\delta v$, follow a similar procedure as previously deriving the linearized wave equation \ref{eq:borneq}. Take derivative of equation \ref{eq:adjointeq} with respect to $m$ and multiply both sides with $\delta m$:
\[
\frac{\partial L^T}{\partial m}v\delta m+L^T\frac{\partial v}{\partial m}\delta m=R\frac{\partial}{\partial m}(d-d_0)\delta m.
\]
Again, recognize that $\frac{\partial L^T}{\partial m}=-\nabla^2$, $\frac{\partial v}{\partial m}\delta m=\delta v$, and the right-hand side:
\[
R\frac{\partial}{\partial m}(d-d_0)\delta m=R\frac{\partial d}{\partial m}\delta m=R\delta d=R^2\delta u=R\delta u,
\]
to arrive at the secondary adjoint equation:
\begin{equation}
L^T\delta v=R\delta u+\nabla^2v\delta m.
\label{eq:secondadjoint}
\end{equation}
\par
The first term on the right-hand side of equation \ref{eq:hessap} is the crosscorrelation of the secondary forward wavefield, $\delta u$, which radiates from the model perturbation, and the adjoint wavefield, $v$, which radiates from the receivers. Consequently, this term generates a sensitivity zone that connects the model perturbation with the receivers (Figure \ref{fig:f2}a). A model perturbation in this zone will have second-order effect on the modeled data.
\par
The second term on the right-hand side of equation \ref{eq:hessap} is the crosscorrelation of the forward wavefield, $u$, which radiates from the source, and the secondary adjoint wavefield, $v$. Different from the secondary forward wavefield, $\delta u$, (equation \ref{eq:borneq}), the secondary adjoint wavefield, $\delta v$, (equation \ref{eq:secondadjoint}) has \textbf{two} source terms. Define two secondary adjoint wavefields, $\delta v_1$ and $\delta v_2$ s.t. $\delta v=\delta v_1+\delta v_2$, that are generated from these two source terms as:
\begin{subequations}
\begin{align}
L^T\delta v_1&=R\delta u,\label{eq:secondadjoint1}\\
L^T\delta v_2&=\nabla^2v\delta m. \label{eq:secondadjoint2}
\end{align}
\end{subequations}
\par
The first source term, $R\delta u$, is the injection of the secondary forward wavefield, $\delta u$, at receiver locations. This source generates a secondary adjoint wavefield, $\delta v_1$, that radiates from the receivers, which, when crosscorrelates with the forward wavefield in the second term on the right-hand side of equation \ref{eq:hessap}, will form a sensitivity zone that connects the source and the receivers (Figure \ref{fig:f2}b right). A model perturbation in this zone will have first-order effect on the modeled data. This first-order effect dominates the Hessian and is defined as the Gauss-Newton Hessian, $H_1$:
\begin{equation}
H_1\delta m=\nabla^2u \cdot \delta v_1.
\label{eq:hessap1}
\end{equation}
Note that this first-order sensitivity zone is similar to the one formed by the gradient, but with a different adjoint source for the adjoint wavefield. In the gradient computation, the adjoint source is the data residual (equation \ref{eq:adjointeq}), which contains all scattering orders. In the application of the Gauss-Newton Hessian, the adjoint source is the scattered forward wavefield (equation \ref{eq:secondadjoint1}), which contains only first-order scattering, hence waveform inversion with a Newton update is less susceptible to cycle skipping.
\par
The second source term in equation \ref{eq:secondadjoint} is the scattering of the adjoint wavefield, $v$, off model perturbation $\delta m$. This source generates a secondary adjoint wavefield, $\delta v_2$, that radiates from the model perturbation, which, when crosscorrelates with the forward wavefield in the second term on the right-hand side of equation \ref{eq:hessap}, will form a sensitivity zone that connects the model perturbation and the source (Figure \ref{fig:f2}b left). A model perturbation in this zone will have second-order effect on the modeled data. 
\par
In total:
\begin{equation}
\begin{aligned}
H\delta m&=\nabla^2u \cdot \delta v_1+\nabla^2u \cdot \delta v_2+\nabla^2\delta u \cdot v,\\
&=H_1\delta m + H_2\delta m
\end{aligned}
\end{equation}
where
\begin{equation}
H_2\delta m=\nabla^2u \cdot \delta v_2+\nabla^2\delta u \cdot v
\label{eq:hessap2}
\end{equation} 
is component of the full Hessian that accounts for second-order scattering effects. Next section will prove this component is similar to the WEMVA operator.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{f2}
\caption{Sensitivity zones from the Hessian. Figure taken from \cite{Fichtner}.}
\label{fig:f2}
\end{figure}

\section{Image and WEMVA operator}
I start by defining the image at zero subsurface offset and WEMVA operator that relates changes in model to changes in this zero subsurface offset image. The images and operators for other subsurface offsets are derived similarly. The image at zero subsurface offset, $I_0$, is defined similarly as the gradient but with the adjoint wavefield, computed by injecting the observed data, not the data residual:
\begin{equation}
I_0=\nabla^2u\cdot \bar{v},
\label{eq:image}
\end{equation}
where $\bar{v}$:
\begin{equation}
L^T\bar{v}=Rd_0.
\label{eq:adjointeq1}
\end{equation}
\par
The WEMVA operator relates changes in the image to changes in the model:
\begin{equation}
\delta I_0=\frac{\partial I_0}{\partial m} \delta m=W_0\delta m.
\end{equation}
Apply chain rules to equation \ref{eq:image}:
\begin{equation}
\delta I_0=\nabla^2\delta u\cdot \bar{v}+\nabla^2u\cdot \delta \bar{v}.
\label{eq:wemva}
\end{equation}
To find $\delta \bar{v}$, take derivative of equation \ref{eq:adjointeq1} with respect to $m$ and multiply both sides with $\delta m$:
\[
\frac{\partial L^T}{\partial m}\bar{v}\delta m+L^T\frac{\partial \bar{v}}{\partial m}\delta m=0,
\]
which, with $\frac{\partial L^T}{\partial m}=-\nabla^2$ and $\frac{\partial \bar{v}}{\partial m}\delta m=\delta \bar{v}$, is equivalent to:
\begin{equation}
L^T\delta \bar{v}=\nabla^2\bar{v}\delta m.
\label{eq:dadjointeq1}
\end{equation}
This equation defines a secondary adjoint wavefield, $\delta \bar{v}$, what is the scattering of the adjoint wavefield, $\bar{v}$, off model perturbation, $\delta m$, which is very similar to how $\delta v_2$ is defined in equation \ref{eq:secondadjoint2}. In fact, substitute $v$ by $\bar{v}$ and $\delta v_2$ by $\delta \bar{v}$, equation \ref{eq:hessap2} that defines the second-order scattering component of the Hessian becomes exactly equation \ref{eq:wemva}, which defines the WEMVA operator. 
\par
The equivalence of the second-order scattering component of the Hessian, $H_2$, and the WEMVA operator, $W_0$, also implies $W_0$ is symmetric. As a result, the computation of the application of $W_0^T$ on an image perturbation, $\delta I_0$, resembles equation \ref{eq:wemva} substituting $\delta I_0$ for $\delta m$:
\begin{equation}
W_0^T\delta I_0=\delta m=\nabla^2\delta u_I\cdot \bar{v}+\nabla^2u\cdot \delta \bar{v}_I,
\end{equation}
where:
\begin{subequations}
\begin{align}
L\delta u_I&=\nabla^2u\delta I_0,\\
L^T\delta \bar{v}_I&=\nabla^2\bar{v}\delta I_0.
\end{align}
\end{subequations}
Now define $I$ to be the extended images at all subsurface offsets, i.e. a column vector of images at different subsurface offsets:
\begin{equation}
I=[I_i].
\label{eq:allimage}
\end{equation}
Images at finite subsurface offset, $I_i$, are computed similarly to $I_0$ (equation \ref{eq:image}) but with the two wavefields shifted spatially. As a result, corresponding WEMVA operators, $W_i$, are defined by spatially shifted cross-correlations and the extended WEMVA operator, $W$, is a column vector of $W_i$:
\begin{subequations}
\begin{align}
\delta I&=[\delta I_i]=[W_i]\delta m=W\delta m,\\
\delta m&=W^T\delta I=[W_i]^T[\delta I_i]=\sum_i W_i\delta I_i.
\end{align}
\end{subequations}

\section{Born series, Jacobian, and Hessian tensor}
A lot of the previously derived operators are closely related to the Born scattering series. See, for example, \cite{Laurent} for a more formal analysis of Born series. Define:
\begin{equation}
m=m_0+\delta m,
\end{equation}
and consider the first three terms of the Taylor series expansion of $u(m)$ around $m_0$:
\begin{equation}
\begin{aligned}
u&=u(m_0)+\frac{\partial u(m_0)}{\partial m}\delta m+\frac{1}{2}\frac{\partial^2 u(m_0)}{\partial m^2}\delta m^2,\\
&=u(m_0)+J(m_0)\delta m+\frac{1}{2}\mathcal{H}(m_0)\delta m^2,\\
&=u_0+u_1+u_2,
\label{eq:bornseries}
\end{aligned}
\end{equation}
where $J$ is the Jacobian matrix, $\mathcal{H}$ is the Hessian tensor, defined by:
\begin{subequations}
\begin{align}
J&=\frac{\partial u}{\partial m},\label{eq:jacobian}\\
\mathcal{H}&=\frac{\partial^2 u}{\partial m^2},\label{eq:hessiantensor}
\end{align}
\end{subequations}
$u_0$ is the background wavefield, $u_1$ is the first-order scattered wavefield, and $u_2$ is the second-order scattered wavefield, defined by:
\begin{subequations}
\begin{align}
u_0&=u(m_0),\label{eq:u0}\\
u_1&=J(m_0)\delta m,\label{eq:u1}\\
u_2&=\frac{1}{2}\mathcal{H}(m_0)\delta m^2.\label{eq:u2}
\end{align}
\end{subequations}
When the data/wavefield space is $\mathbb{R}^{N_d}$ and the model space is $\mathbb{R}^{N_m}$, the Jacobian matrix is in $\mathbb{R}^{N_d\times N_m}$ and relates a change in model to a first-order change in wavefield. The Hessian tensor is a third-order tensor in $\mathbb{R}^{N_d\times N_m\times N_m}$ and relates a change in model to a second-order change in wavefield.
\par
Let's now derive equations for the wavefields $u_i$. The background wavefield satisfies the wave equation \ref{eq:waveeq} with the background model $m_0$: 
\begin{equation}
L(m_0)u_0=s.
\end{equation}
Take the first and second derivatives of the wave equation \ref{eq:waveeq} with respect to model $m$ to get:
\begin{subequations}
\begin{align}
\frac{\partial L}{\partial m}u+L\frac{\partial u}{\partial m}&=0,\label{eqa}\\
\frac{\partial^2 L}{\partial m^2}u+2\frac{\partial L}{\partial m}\frac{\partial u}{\partial m}&+L\frac{\partial^2 u}{\partial m^2}=0.\label{eqb}
\end{align}
\end{subequations}
Using the definitions \ref{eq:u1}, \ref{eq:u2}, $\frac{\partial L}{\partial m}=-\nabla^2$ and $\frac{\partial^2 L}{\partial m^2}=0$, multiply both sides of equation \ref{eqa} with $\delta m$ and equation \ref{eqb} with $\frac{1}{2}\delta m^2$ and evaluate both equations at $m_0$ to arrive at:
\begin{subequations}
\begin{align}
L(m_0)u_1&=\nabla^2 u_0\delta m,\label{eq:u1eq}\\
L(m_0)u_2&=\nabla^2 u_1\delta m,\label{eq:u2eq}
\end{align}
\end{subequations}
or in general, for the $n$\textsuperscript{th}-order scattered wavefield $u_n$:
\begin{equation}
L(m_0)u_n=\nabla^2 u_{n-1}\delta m.
\end{equation}
\par
Notice that equation \ref{eq:u1eq} for the first-order scattered wavefield is exactly the same as the linearized wave equation \ref{eq:borneq}, therefore $u_1=\delta u$. Consequently:
\begin{equation}
B\delta m=\delta d=R\delta u=Ru_1=RJ(m_0)\delta m,
\end{equation}
which establishes the relationship between the Born operator and the Jacobian matrix:
\begin{subequations}
\begin{align}
B&=RJ,\label{eq:B}\\
\frac{\partial B}{\partial m}&=R\frac{\partial J}{\partial m}=R\mathcal{H}.\label{eq:Bm}
\end{align}
\end{subequations}
\par
To see how the Hessian of the objective function and the Hessian tensor are related, take derivative of equation \ref{eq:obj} with respect to $m$ twice:
\begin{equation}
\begin{aligned}
H=\frac{\partial^2 f}{\partial m^2}=&\left(\frac{\partial d}{\partial m}\right)^T\left(\frac{\partial d}{\partial m}\right)+(d-d_0)\left(\frac{\partial^2 d}{\partial m^2}\right),\\
=&B^TB+(d-d_0)R\mathcal{H}.
\end{aligned}
\end{equation}
Recognize that the first term on the right-hand side of the above equation is the Gauss-Newton Hessian, so:
\begin{subequations}
\begin{align}
H_1&=B^TB=J^TJ,\\
H_2&=(d-d_0)\frac{\partial B}{\partial m}=(d-d_0)R\mathcal{H}. \label{WEMVAandB}
\end{align}
\end{subequations}

\section{Tomographic operator}
Recall that the Born operator relates a change in model to a change in data taking into account only first-order scattering. The tomographic operator, on the other hand, relates a change in model to a change in data taking into account both first and second-order scattering. From equation \ref{eq:bornseries}, the first and second-order scattering wavefields are $u_1$ and $u_2$, hence we can write the tomographic operator as:
\begin{subequations}
\begin{align}
T\delta m&=Ru_{12}=R(u_1+u_2)=B\delta m+\frac{1}{2}R\mathcal{H}\delta m^2,\\
T&=B+\frac{1}{2}R\mathcal{H}\delta m.
\label{eq:tomo}
\end{align}
\end{subequations}
where $u_{12}=u_1+u_2$ is the solution to the summed equation of \ref{eq:u1eq} and \ref{eq:u2eq} (i.e. scattering of the background and first-order scattered wavefields, $u_{01}=u_0+u_1$, off model perturbation $\delta m$):
\begin{equation}
Lu_{12}=\nabla^2 u_{01}\delta m.
\label{eq:u12eq}
\end{equation}
As a result, we can write:
\begin{equation}
u_{12}=L^{-1}\nabla^2u_{01}\delta m,
\label{eq:u12}
\end{equation}
From and equations \ref{eq:tomo} and \ref{eq:u12}, we get an expression for the tomographic operator:
\begin{equation}
T=RL^{-1}\nabla^2u_{01}.
\label{eq:tomo1}
\end{equation}
\par
Consider the following objective function:
\begin{equation}
f_1(m)=\frac{1}{2}\left(d+\frac{1}{2}B\delta m-d_0\right)^2.
\label{eq:obj1}
\end{equation}
The above objective function is approximately equal to the objective function $f(m)$ defined in \ref{eq:obj} and is slightly different (by the factor $\frac{1}{2}$ in $\frac{1}{2}B\delta m$) from the least-square RTM objective function. I'll show that derivative of this newly defined objective function involve the adjoint tomographic operator. Using $\frac{\partial d}{\partial m}=B$ (equation \ref{eq:born}), $\frac{\partial B}{\partial m}=R\mathcal{H}$ (equation \ref{eq:Bm}), and $T=B+\frac{1}{2}R\mathcal{H}\delta m$ (equation \ref{eq:tomo}):
\begin{equation}
\begin{aligned}
\nabla f_1(m)&=\left[ \left(\frac{\partial d}{\partial m}\right)^T+\frac{1}{2}\delta m\left(\frac{\partial B}{\partial m}\right)^T\right]\left(d+\frac{1}{2}B\delta m-d_0\right),\\
&=(B^T+\frac{1}{2}\delta m \mathcal{H}^TR)\left(d+\frac{1}{2}B\delta m-d_0\right),\\
&=T^T\left(d+\frac{1}{2}B\delta m-d_0\right).
\end{aligned}
\end{equation}
Now using $T=RL^{-1}\nabla^2u_{01}$ (equations \ref{eq:tomo1}), $B\delta m=R\delta u$ (equation \ref{eq:born1}), $v=L^{-T}R(d-d_0)$ (equation \ref{eq:adjointeq}), and $\delta v_1=L^{-T}R\delta u$ (equation \ref{eq:secondadjoint1}):
\begin{equation}
\begin{aligned}
\nabla f_1(m)&=\nabla^2u_{01}\cdot L^{-T}R\left(d+\frac{1}{2}R\delta u-d_0\right),\\
&=\nabla^2u_{01}\cdot\left[L^{-T}R(d-d_0)+\frac{1}{2}L^{-T}R\delta u\right],\\
&=\nabla^2u_{01}\cdot\left(v+\frac{1}{2}\delta v_1\right).
\end{aligned}
\end{equation}

\section{RFWI=LSRTM+WEMVA}
Reflection FWI defines the following objective function
\begin{equation}
f(b,r)=\frac{1}{2}\left[B(b)r-\Delta d\right]^2,
\end{equation}
in which the model has been explicitly decomposed into the background and reflectivity components $m=b+r$ and $\Delta d=d_0-d(b)$. Gradient with respect to reflectivity is the usual LSRTM gradient
\begin{equation}
\frac{\partial f}{\partial r}=B(b)^T\left[B(b)r-\Delta d\right],
\end{equation}
while gradient with respect to background model is
\begin{equation}
\frac{\partial f}{\partial b}=r\left(\frac{\partial B}{\partial b}\right)^T\left[B(b)r-\Delta d\right].
\end{equation} 
Relating the right hand side to equation \ref{WEMVAandB}, one relizes this is nothing but WEMVA or more specifically the WEMVA part of the full Hessian, $H_2$, with slightly different residual and perturbation
\begin{equation}
\frac{\partial f}{\partial b}=\nabla^2u \cdot \delta\tilde{v} + \nabla^2\delta\tilde{u} \cdot \tilde{v},
\end{equation}
with 
\begin{subequations}
\begin{align}
Lu&=s,\\
L\delta\tilde{u}&=\nabla^2ur,\\
L^T\tilde{v}&=B(b)r-\Delta d,\\
L^T\delta\tilde{v}&=\nabla^2\tilde{v}r.
\end{align}
\end{subequations}

\section{Summary}
\subsection{Definitions}
\begin{equation}
L=\partial_t^2-m\nabla^2
\end{equation}
\begin{equation}
Lu=s
\end{equation}
\begin{equation}
d=Ru=\begin{cases}
     u(x_r,t) \mbox{ for } x=x_r\\
     0 \mbox{ otherwise}
     \end{cases}
\end{equation}
\begin{equation}
f(m)=\frac{1}{2}(d-d_0)^2
\end{equation}

\subsection{Born operator}
\begin{subequations}
\begin{align}
B\delta m&=\delta d=R\delta u\\
L\delta u&=\nabla^2u\delta m
\end{align}
\end{subequations}

\subsection{Gradient}
\begin{subequations}
\begin{align}
\nabla_m f&=B^T(d-d_0)=\nabla^2u \cdot v\\
L^Tv&=R(d-d_0)
\end{align}
\end{subequations}

\subsection{Hessians}
\begin{subequations}
\begin{align}
H\delta m&=\nabla^2\delta u \cdot v + \nabla^2u \cdot \delta v\\
L^T\delta v&=R\delta u+\nabla^2v\delta m
\end{align}
\end{subequations}
\begin{subequations}
\begin{align}
H_1\delta m&=\nabla^2u \cdot \delta v_1\\
L^T\delta v_1&=R\delta u
\end{align}
\end{subequations}
\begin{subequations}
\begin{align}
H_2\delta m&=\nabla^2u \cdot \delta v_2+\nabla^2\delta u \cdot v\\
L^T\delta v_2&=\nabla^2v\delta m
\end{align}
\end{subequations}

\subsection{Image and WEMVA}
\begin{subequations}
\begin{align}
I_i&=\nabla^2u\cdot \bar{v}\\
L^T\bar{v}&=Rd_0
\end{align}
\end{subequations}
\begin{subequations}
\begin{align}
W_i\delta m=\delta I_i&=\nabla^2\delta u\cdot \bar{v}+\nabla^2u\cdot \delta \bar{v}\\
L^T\delta \bar{v}&=\nabla^2\bar{v}\delta m
\end{align}
\end{subequations}
\begin{subequations}
\begin{align}
W_i^T\delta I_i=\delta m&=\nabla^2\delta u_I\cdot \bar{v}+\nabla^2u\cdot \delta \bar{v}_I\\
L\delta u_I&=\nabla^2u\delta I_i\\
L^T\delta \bar{v}_I&=\nabla^2\bar{v}\delta I_i
\end{align}
\end{subequations}
\begin{subequations}
\begin{align}
\delta I&=[\delta I_i]=[W_i]\delta m=W\delta m\\
\delta m&=W^T\delta I=[W_i]^T[\delta i]=\sum_i W_i\delta I_i
\end{align}
\end{subequations}

\subsection{Born series, Jacobian, and Hessian tensor}
\begin{subequations}
\begin{align}
u=u_0+u_1&+u_2+...+u_{n+1}\\
L(m_0)u_0&=s\\
L(m_0)u_1&=\nabla^2 u_0\delta m\\
L(m_0)u_2&=\nabla^2 u_1\delta m\\
L(m_0)u_{n+1}&=\nabla^2 u_n\delta m
\end{align}
\end{subequations}
\begin{subequations}
\begin{align}
u_0&=u(m_0)\\
u_1&=J(m_0)\delta m\\
u_2&=\frac{1}{2}\mathcal{H}(m_0)\delta m^2\\
J&=\frac{\partial u}{\partial m}\\
B&=RJ\\
\mathcal{H}&=\frac{\partial^2 u}{\partial m^2}\\
\frac{\partial B}{\partial m}&=R\mathcal{H}\\
H&=J^TJ+(d-d_0)R\mathcal{H}
\end{align}
\end{subequations}

\subsection{Tomographic operator}
\begin{subequations}
\begin{align}
T&=B+\frac{1}{2}R\mathcal{H}\delta m\\
T\delta m&=R(u_1+u_2)\\
f_1(m)&=\frac{1}{2}\left(d+\frac{1}{2}B\delta m-d_0\right)^2\\
\nabla f_1(m)&=\nabla^2(u_0+u_1)\cdot\left(v+\frac{1}{2}\delta v_1\right)
\end{align}
\end{subequations}

\subsection{RFWI}
\begin{subequations}
\begin{align}
f(b,r)&=\frac{1}{2}\left[B(b)r-\Delta d\right]^2,\\
\frac{\partial f}{\partial r}&=B(b)^T\left[B(b)r-\Delta d\right],\\
\frac{\partial f}{\partial b}&=\nabla^2u \cdot \delta\tilde{v} + \nabla^2\delta\tilde{u} \cdot \tilde{v},\\
Lu&=s,\\
L\delta\tilde{u}&=\nabla^2ur,\\
L^T\tilde{v}&=B(b)r-\Delta d,\\
L^T\delta\tilde{v}&=\nabla^2\tilde{v}r.
\end{align}
\end{subequations}

\medskip
 
\begin{thebibliography}{4}
\bibitem{Ali}
Almomin, A., 2013, Accurate implementation of two-way wave-equation operators, SEP149, 279-286.
\bibitem{Guillaume}
Barnier, G. and A. Almomin, 2014, Tutorial on two-way wave equation operators for acoustic isotropic constant-density media, SEP155, 25-58.
\bibitem{Biondo}
Biondi, B., 3D Seismic Imaging class notes.
\bibitem{Fichtner} 
Fichtner, A. and J. Trampert, 2011, Hessian kernels of seismic data functionals based on adjoint techniques, Geophysical Journal International, 185, 775-798. 
\bibitem{Laurent}
Laurent Demanet. 18.325 Topics in Applied Mathematics: Waves and Imaging. Fall 2012. Massachusetts Institute of Technology: MIT OpenCourseWare, \url{https://ocw.mit.edu}. License: Creative Commons BY-NC-SA.

\end{thebibliography}

\end{document}  
